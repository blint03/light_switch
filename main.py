#Created by Blinster

import machine
import time
import utime
import fireflies, colorwave, solid, breath

light_switch = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)

time_last = time.ticks_ms()
i=0
org_state = 0 

print(f"main script started, button value = {light_switch.value()}")

def button_handler(pin):
    global time_last, light_switch, i, org_state
    
    if time.ticks_diff(time.ticks_ms(), time_last) > 1200: #button debouncer
        time_last = time.ticks_ms()
        print(f"debouncer activated, light_switch value= {light_switch.value()}")

        if org_state == 0:
            i+=1
            time.sleep(.5)
            print(f"interval= {i}")
            
        org_state = light_switch.value()
            
        if i == 1:
            print("fireflies starting")
            fireflies.lights()
    
        elif i == 2:
            print("colorwave starting")
            colorwave.lights()
    
        elif i == 3:
            print("breath starting")
            breath.lights()
    
        elif i == 4:
            print("solid starting")
            solid.lights()
        
        else:
            print("resetting i")
            i = 1
            fireflies.lights()
            
    
light_switch.irq(trigger = machine.Pin.IRQ_FALLING, handler = button_handler)
