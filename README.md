# Light_switch

python code for raspberry pico powered NeoPixel Ring. 

## Step 0 - Shopping List
1. 1x NeoPixel Ring - 16 x 5050 RGB LED with Integrated Drivers - https://www.adafruit.com/product/1463
2. 1x DIY Magnetic Connector - Right Angle Four Contact Pins - https://www.adafruit.com/product/5358
3. 1x Stranded-Core Wire Spool - 25ft - 22AWG - (ideally red, black, blue) - https://www.adafruit.com/product/2976
4. 1x Raspberry Pi Pico RP2040 w/ or w/o Loose Unsoldered Headers - https://www.adafruit.com/product/4883
5. 1x 5V 2.5A Switching Power Supply with 20AWG MicroUSB Cable - https://www.adafruit.com/product/1995
6. 2x Side-light Fiber Optic Tube - 4mm Diameter - 1 meter long - https://www.adafruit.com/product/4163
7. 1x SparkFun Accessories Panel Mount USB Micro-B Extension Cable - 6in.- https://www.mouser.com/ProductDetail/SparkFun/CAB-15464?qs=XeJtXLiO41TW%252BnYEDtaNzg%3D%3D
8. 1x Rare Earth 3/4 in. x 1/8 in. Disc Magnet - (Home Depot) https://tinyurl.com/y6suu97s
9. 1x Miscellaneous washer or metal disk roughly 19mm in diameter and 3mm in depth. 
**Note:** Putting magnets on both sides was way too strong. If you'd like your light to come out of the holder only use one magnet.
10. Adhesive - Examples: 3D Gloop, Loctite, Gorilla Glue, etc. 
11. Optional: Heat shrink or electrical tape

## Step 1 - Print Top

**Caution:** *Do not use pliers or any other tool to force fiber optic tube, it will damage it.*

Print Top. 

**Note:** *Color switch can be done to allow light to shine through transparent top material.*

Recommended Print Settings: 
 - Infill: 20%
 - Parameters: 3
 - Layer height: .15mm
 - Supports: None

Ensure fiber optic tube fits snuggly (but not too snuggly) into receptacle. If you have problems getting fiber to fit, recommend using a Dremel tool or small nail file to remove any internal burrs. I haven't confirmed it would work but scaling it up by ~1% may also solve that problem.

## Step 2 - Print support legs
Print support legs and make sure the legs fit snuggly within the top. Hold off on gluing these in right now.

Recommended Print Settings: 
 - Infill: 40%
 - Parameters: 3
 - Layer height: .20mm
 - Supports: None

## Step 3 - Print Bottom Fiber Optics Holder
Print bottom fiber optic holder and make sure legs fit snuggly within the bottom. Hold off on gluing legs in right now. 

Recommended Print Settings: 
 - Infill: 20%
 - Parameters: 3
 - Layer height: .15mm
 - Supports: Base plate only

## Step 4 - Measure and cut fiber optic tube
There are many ways to run the fiber optic tube here. As shown in the picture mine has every other strand going straight and the rest rotate x places. The length of mine are:

 - 8x Zmm 
 - 8x Ymm

**Note:** *There should be enough fiber optic for a mistake or two but take care when measuring and cutting.* 

## Step 5 - Print Top & Bottom Electronic Plate
**Caution:** *The Top and Bottom Electronic Plates can fit very tightly into the other parts so if you test fit these proceed with caution. Having the magnet and washer installed may help extricate a stuck part but it's not guaranteed.*

Print Top and Bottom Electronic Plates. 

Recommended Print Settings: 
 - Infill: 20%
 - Parameters: 3
 - Layer height: .20mm
 - Supports: None

Press fit magnetic connectors in Plates. Make sure proper polarity of magnets in connector before progressing. These magnetic connectors are not reversable! 

**Note:** *Magnetic connectors should fit snuggly. If for some reason it is lose refrain from gluing until after you've tested alignment, soldered the wires, and preferably tested the electronics.* 

## Optional: Initial Electronics Test
At this point I'd highly recommend testing your Pico and NeoPixel ring setup. Also, if you are not familiar with these components below are a couple good resources to get better acquainted:
 
 - https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html
 - https://makersportal.com/blog/ws2812-ring-light-with-raspberry-pi-pico
 - https://bhave.sh/micropython-neopixels-1/

A simple wiring setup to test the Pico and NeoPixels would look something like this:
[insert image]

**Note:** *Since this only has 16 pixels and limited wiring distance, I leveraged the 3v3_EN pin to power the LEDs. This seemed to work fine at max brightness and prevented the hassle of having a dedicated power supply.* 

If you have a set of alligator clips you can test this setup with the magnetic connectors attached to the Top and Bottom plates as well. Just be careful not to let the pins hit each other.

## Step 6 - Place Magnet and Washer  
Glue magnet into magnet slot on Top Electronic Plate. Glue washer or something of comparable size in the bottom Bottom Electronic Plate. Two magnets seemed like overkill for this, it just needs to provide a satisfying pull and click when putting the light in the base plate. 

## Step 7 - Install NeoPixel Ring Electronics
Line the NeoPixel Ring up with the fiber optic tube receptacles (aka holes) and liberally apply adhesive around the outside of the ring to ensure it stays put. Ensure you do not glue over the Data Input, Power Signal Ground, or Power 5v DC connectors on the ring. Before setting aside to dry, double check holes align well with the lights. If ring rotates out of alignment the light will not go through fiber optic tube well.  

**Note:** *When measuring the wire necessary for the magnetic connector to the ring play it safe and give yourself a little extra length. If you use stranded core wires they should be flexible enough to coil up inside. After soldering the wires to the magnetic connector test fit the wires into the ring and see if it closes completely. If not, trim it down until it fits.* 

Solder wires to the magnetic connector on the Top Plate in the following order: 
[insert picture]

**Note:** Soldering the two pins together on the ground is essential on the Top Electronic Plate. It will allow the connector to act similar to a button push when you add another data wire to the Bottom Electronic Plate. This will allow you to transition through the different color schemes by simply pulling off the base and replacing it, rather than requiring buttons. More on this later. 

Use heat shrink, electrical tape, and/or apply a nonconductive adhesive over pins to prevent them from rotating and making contact with each other. This is an important step as these things have a tendency to turn. 

Solder the appropriate wires to the NeoPixel Ring.
[insert picture]

## Step 8 - Print Base
Print Base.

Recommended Print Settings: 
 - Infill: 20%
 - Parameters: 3
 - Layer height: .20mm
 - Supports: Top of the inner power cable slot. Might need support blockers on outside to prevent them from being created on outside of the case. 

[insert picture]

Apply adhesive to Panel Mount Cable and insert into provided cutout, making sure it is oriented up correctly. Once dry, wrap the cable around like the picture below to allow room for Pico to squeeze in:
[insert picture]

## Step 9 - Base Electronics
Solder wires to the magnetic connector on the Bottom Plate in the following order:
[insert picture]

Solder wires to the Pico as shown below:
[insert picture]

## Optional: Final Electronics Test

## Step 10 - Assemble 
**Caution:** *Make sure to provide adequate time for glue to dry and let parts dry separately, rather than completely assembled. On several occasions I had fumes from the adhesives fuse pieces together that shouldn't have been.* 
   
Glue Top Electronics Plate to Bottom Fiber Optics Holder.
Glue Bottom Electronics Plate to supports inside Base.
Glue Legs into Bottom Fiber Optics Holder and Top
Install Fiber Optic Tubing

## Step 11 - Code
