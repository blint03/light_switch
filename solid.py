# Based on the neopixel_ring example at:
# https://github.com/raspberrypi/pico-micropython-examples/tree/master/pio/neopixel_ring
# Remixed by Blintster

import array, time
import machine
import rp2

#
############################################
# RP2040 PIO and Pin Configurations
############################################
#
# WS2812 LED Ring Configuration
led_count = 16 # number of LEDs in ring light
PIN_NUM = 13 # pin connected to ring light
brightness = .4 # 0.1 = darker, 1.0 = brightest
light_switch = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)

@rp2.asm_pio(sideset_init=rp2.PIO.OUT_LOW, out_shiftdir=rp2.PIO.SHIFT_LEFT,
             autopull=True, pull_thresh=24) # PIO configuration

# define WS2812 parameters
def ws2812():
    T1 = 2
    T2 = 5
    T3 = 3
    wrap_target()
    label("bitloop")
    out(x, 1)               .side(0)    [T3 - 1]
    jmp(not_x, "do_zero")   .side(1)    [T1 - 1]
    jmp("bitloop")          .side(1)    [T2 - 1]
    label("do_zero")
    nop()                   .side(0)    [T2 - 1]
    wrap()


# Create the StateMachine with the ws2812 program, outputting on pre-defined pin
# at the 8MHz frequency
state_mach = rp2.StateMachine(0, ws2812, freq=8_000_000, sideset_base=machine.Pin(PIN_NUM))

# Activate the state machine
state_mach.active(1)

# Range of LEDs stored in an array
pixel_array = array.array("I", [0 for _ in range(led_count)])
#
############################################
# Functions for RGB Coloring
############################################
#

def update_pix(brightness_input=brightness): # dimming colors and updating state machine (state_mach)
    dimmer_array = array.array("I", [0 for _ in range(led_count)])
    for ii,cc in enumerate(pixel_array):
        r = int(((cc >> 8) & 0xFF) * brightness_input) # 8-bit red dimmed to brightness
        g = int(((cc >> 16) & 0xFF) * brightness_input) # 8-bit green dimmed to brightness
        b = int((cc & 0xFF) * brightness_input) # 8-bit blue dimmed to brightness
        dimmer_array[ii] = (g<<16) + (r<<8) + b # 24-bit color dimmed to brightness
    state_mach.put(dimmer_array, 8) # update the state machine with new colors
    time.sleep_ms(10)

def set_24bit(ii, color): # set colors to 24-bit format inside pixel_array
    pixel_array[ii] = (color[1]<<16) + (color[0]<<8) + color[2] # set 24-bit color

def lights():
    while True:     
        for jj in inner_leds:
            set_24bit(jj, inner_color) # 
        
        for jj in outer_leds:
            set_24bit(jj, outer_color)
        
        update_pix()
        
        if light_switch.value() == 1:
            print(f"solid stopping")
            break

#
############################################
# Main Loops and Calls
############################################
#

blank = (0,0,0) 
blue = (56,172,236)
yellow = (255,255,0)
purple = (125,7,126)
green = (0,128,0)
pink = (229,84,81)
red = (255, 0, 0)

inner_leds = [1,3,5,7,9,11,13,15]
outer_leds = [0,2,4,6,8,10,12,14]
inner_color = pink
outer_color = green
