# Based on colorwave example at: https://github.com/blaz-r/pi_pico_neopixel
# Remixed by Blintster

import time
import machine
from neopixel import Neopixel

light_switch = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP)
numpix = 16
strip = Neopixel(numpix, 0, 13, "GRB")


red = (255, 0, 0)
orange = (255, 50, 0)
yellow = (255, 100, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
indigo = (100, 0, 90)
violet = (200, 0, 100)
colors_rgb = [red, orange, yellow, green, blue, indigo, violet]

# same colors as normaln rgb, just 0 added at the end
colors_rgbw = [color+tuple([0]) for color in colors_rgb]
colors_rgbw.append((0, 0, 0, 255))

# uncomment colors_rgbw if you have RGBW strip
colors = colors_rgb
# colors = colors_rgbw


step = round(numpix / len(colors))
current_pixel = 0
strip.brightness(100)

for color1, color2 in zip(colors, colors[1:]):
    strip.set_pixel_line_gradient(current_pixel, current_pixel + step, color1, color2)
    current_pixel += step

strip.set_pixel_line_gradient(current_pixel, numpix - 1, violet, red)

def lights():
    while True:
        strip.rotate_right(1)
        time.sleep(0.08)
        strip.show()
        
        if light_switch.value() == 1:
            print(f"colorwave stopping")
            break